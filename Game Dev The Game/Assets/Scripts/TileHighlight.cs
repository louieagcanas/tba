﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileHighlight
{
	public static List<Tile> FindHighlight(Tile p_originTile, int p_movementPoints)
	{
		List<Tile> closed = new List<Tile>();
		List<TilePath> open = new List<TilePath>();

		TilePath originPath = new TilePath();
		originPath.AddTile( p_originTile );

		open.Add( originPath );

		while( open.Count > 0 )
		{
			TilePath current = open[0];
			open.Remove( open[0] );

			if( closed.Contains( current.LastTile ) )
			{
				continue;
			}

			if( current.CostOfPath > p_movementPoints + 1 )
			{
				continue;
			}

			closed.Add( current.LastTile );

			foreach( Tile t in current.LastTile.Neighbors )
			{
				if( t.IsImpassable ) continue;
				if( t.IsOccupied ) continue;

				TilePath tilePath = new TilePath( current );
				tilePath.AddTile( t );
				open.Add( tilePath );
			}
		}

		closed.Remove( p_originTile );
		return closed;
	}
}
