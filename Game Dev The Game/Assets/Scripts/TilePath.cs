﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TilePath 
{
	private List<Tile> m_listOfTiles = new List<Tile>();
	private Tile m_lastTile;
	private int m_costOfPath = 0;
	
	public List<Tile> ListOfTiles
	{
		get{ return m_listOfTiles; }
	}
	
	public Tile LastTile
	{
		get{ return m_lastTile; }
	}
	
	public int CostOfPath
	{
		get{ return m_costOfPath; }
	}

	public TilePath()
	{

	}
	
	public TilePath( TilePath p_tilePath )
	{
		m_listOfTiles = p_tilePath.ListOfTiles.ToList();
		m_lastTile = p_tilePath.LastTile;
		m_costOfPath = p_tilePath.CostOfPath;
	}
	
	public void AddTile( Tile p_tile )
	{
		m_costOfPath += p_tile.MovementCost;
		m_listOfTiles.Add( p_tile );
		m_lastTile = p_tile;
	}
}
