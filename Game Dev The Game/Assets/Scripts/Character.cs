﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character : MonoBehaviour 
{
	private GameManager.Team m_team;

	public int m_hp                         = 500;
	public int m_damage                     = 100;
	public int m_attackRange                = 2;
	public int m_movementDistance           = 5;
	public float m_movementSpeed            = 10.0f;
	public int m_movementCost				= 1;

	private Vector2 m_gridPosition          = Vector2.zero;
    private List<Vector3> m_positionQueue   = new List<Vector3>();
	private bool m_isMoving                 = false;
    private bool m_didMove                  = false;
    private bool m_didAttackOrUseSkill      = false;

	public GameManager.Team Team
	{
		get{ return m_team; }
		set{ m_team = value; }
	}

    public Vector2 GridPosition
    {
        get{ return m_gridPosition; }
        set{ m_gridPosition = value; }
    }
    
    public List<Vector3> PositionQueue
    {
        get{ return m_positionQueue; }
    }

    public bool IsMoving
    {
        get{ return m_isMoving; }
    }

    public bool DidMove
    {
        get{ return m_didMove; }
    }

    public bool DidAttackOrUseSkill
    {
        get{ return m_didAttackOrUseSkill; }
    }

	public int MovementCost
	{
		get{ return m_movementCost; }
	}

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		TurnUpdate();
	}

	public void Move()
	{
		m_isMoving = true;
	}

	public void TurnUpdate()
	{
		if( m_positionQueue.Count > 0 )
		{
			this.transform.position += ( m_positionQueue[0] - this.transform.position ).normalized * m_movementSpeed * Time.deltaTime;
				
			if( Vector3.Distance( m_positionQueue[0], this.transform.position ) <= 0.1f )
			{
				this.transform.position = m_positionQueue[0];
				m_positionQueue.RemoveAt( 0 );
				if( m_positionQueue.Count == 0 )
				{
					this.renderer.material.color = Color.gray;
					m_didMove = true;
					m_isMoving = false;
				}
			}
		}
	}

	void OnGUI()
	{
		Vector3 currentPosition = Camera.main.WorldToScreenPoint( this.transform.position );
		GUI.Label( new Rect( currentPosition.x - 20, Screen.height - currentPosition.y, 40, 20 ), "Louie" );
	}

	void OnMouseEnter()
	{
		//GameManager.Instance.HighlightTilesAt( m_currentGridPosition, Color.green, m_movementDistance );
	}

	void OnMouseExit()
	{
		//GameManager.Instance.RemoveTileHighlights();
	}

	void OnMouseDown()
	{
		if( m_team == GameManager.Instance.CurrentTeam && !m_didMove )
		{
			m_isMoving = true;
			GameManager.Instance.SelectedCharacter = this;
			GameManager.Instance.RemoveTileHighlights();
			GameManager.Instance.HighlightTilesAt( m_gridPosition, Color.green, m_movementDistance );
		}
	}
}
