﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
	public static GameManager Instance;

	public enum Team
	{
		Red 	= 0,
		Blue 	= 1
	}

	public GameObject m_tilePrefab;
	public GameObject m_characterPrefab;
	public int m_columnNumbers;
	public int m_rowNumbers;

	private List<List<Tile>> m_map = new List<List<Tile>>();
	private List<Character> m_characters = new List<Character>();

	private Character m_selectedCharacter;
	private int m_currentTurn = 0;
	private Team m_currentTeam;
	private int m_currentToken = 0;
	private int m_tokenLimit = 1;

	public int ColumNumbers
	{
		get{ return m_columnNumbers; }
	}

	public int RowNumbers
	{
		get{ return m_rowNumbers; }
	}

	public List<List<Tile>> Map
	{
		get{ return m_map; }
	}
	
	public List<Character> Characters
	{
		get{ return m_characters; }
	}

	public Character SelectedCharacter
	{
		get{ return m_selectedCharacter; }
		set{ m_selectedCharacter = value; }
	}

	public Team CurrentTeam
	{
		get{ return m_currentTeam; }
	}

	void Awake()
	{
		Instance = this;
	}

	// Use this for initialization
	void Start () 
	{
		GenerateMap();
		GenerateCharacters();

		m_currentTeam = Team.Blue;
		m_currentTurn++;
		m_currentToken = m_tokenLimit;
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

    void OnGUI()
    {
		GUI.Label( new Rect( Screen.width - 100, 0, 100, 25 ), "Token Count: " + m_currentToken );
		GUI.Label( new Rect( Screen.width - 150, 25, 150, 25 ), "Current Team: " + m_currentTeam );

		if( GUI.Button( new Rect( Screen.width - 100, Screen.height - 25, 100, 25 ), "End Turn" ) )
		{
			UpdateTurn();
		}
    }

	void GenerateMap()
	{
		m_map = new List<List<Tile>>();

		for( int i = 0; i < m_columnNumbers; i++ )
		{
			List<Tile> row = new List<Tile>();

			for( int j = 0; j < m_rowNumbers; j++ )
			{
				GameObject tileGameObject = Instantiate( m_tilePrefab,
				                                        new Vector3( i - Mathf.Floor( m_columnNumbers / 2 ), -j + Mathf.Floor( m_rowNumbers / 2 ), 0.0f ),
				                                        Quaternion.Euler(new Vector3()) ) as GameObject;
                
				tileGameObject.transform.parent = this.transform;

				Tile tile = tileGameObject.GetComponent<Tile>();
                tile.GridPosition = new Vector2( i, j );

				row.Add( tile );
			}

			m_map.Add(row);
		}
	}

	void GenerateCharacters()
	{
		//Blue Team
		for( int i = 0; i < m_rowNumbers; i++ )
		{
			GameObject characterGameObject = Instantiate( m_characterPrefab, 
			                                             new Vector3( 0 - Mathf.Floor( m_columnNumbers / 2 ), -i + Mathf.Floor ( m_rowNumbers / 2 ), -0.9f ),
			                                             Quaternion.identity ) as GameObject;

			characterGameObject.renderer.material.color = Color.blue;
			characterGameObject.transform.parent = this.transform;

			Character character = characterGameObject.GetComponent<Character>();
			character.GridPosition = new Vector2( 0, i );
			m_map[0][i].IsOccupied = true;
			character.Team = Team.Blue;

			m_characters.Add( character );
		}

		//Red Team
		for( int i = 0; i < m_rowNumbers; i++ )
		{
			GameObject characterGameObject = Instantiate( m_characterPrefab, 
			                                             new Vector3( ( m_columnNumbers - 1 ) - Mathf.Floor( m_columnNumbers / 2 ), -i + Mathf.Floor ( m_rowNumbers / 2 ), -0.9f ),
			                                             Quaternion.identity ) as GameObject;
			
			characterGameObject.renderer.material.color = Color.red;
			characterGameObject.transform.parent = this.transform;
			
			Character character = characterGameObject.GetComponent<Character>();
			character.GridPosition = new Vector2( m_columnNumbers - 1, i );
			m_map[m_columnNumbers - 1][i].IsOccupied = true;
			character.Team = Team.Red;
			
			m_characters.Add( character );
		}
	}

	public void UpdateTurn()
	{
		RemoveTileHighlights();

		if( m_currentTeam == Team.Blue )
		{
			m_currentTeam = Team.Red;
		}
		else if( m_currentTeam == Team.Red )
		{
			m_currentTeam = Team.Blue;
			m_currentTurn++;

			if( m_tokenLimit < 10 )
			{
				m_tokenLimit++;
			}
		}

		m_currentToken = m_tokenLimit;
	}

	public void HighlightTilesAt( Vector2 p_originLocation, Color p_highlightColor, int p_movementDistance )
	{
		List<Tile> highlightedTiles = TileHighlight.FindHighlight( m_map[(int)p_originLocation.x][(int)p_originLocation.y], p_movementDistance );

		foreach( Tile t in highlightedTiles )
		{
			t.renderer.material.color = p_highlightColor;
		}
	}

	public void RemoveTileHighlights()
	{
		for( int i = 0; i < m_columnNumbers; i++ )
		{
			for(int j = 0; j < m_rowNumbers; j++ )
			{
				m_map[i][j].renderer.material.color = Color.white;
			}
		}
	}

	public void MoveCharacter(Tile p_tile)
	{
		if( p_tile.renderer.material.color != Color.white )
		{
			if( m_selectedCharacter.MovementCost <= m_currentToken )
			{
				m_currentToken -= m_selectedCharacter.MovementCost;

				m_map[(int)SelectedCharacter.GridPosition.x][(int)SelectedCharacter.GridPosition.y].IsOccupied = false;
				RemoveTileHighlights();

				foreach( Tile t in TilePathFinder.FindPath( m_map[(int)m_selectedCharacter.GridPosition.x][(int)m_selectedCharacter.GridPosition.y], p_tile ) )
				{
	               	m_selectedCharacter.PositionQueue.Add( m_map[(int)t.GridPosition.x][(int)t.GridPosition.y].transform.position + 0.9f * -Vector3.forward );
				}

	            m_selectedCharacter.GridPosition = p_tile.GridPosition;
				p_tile.IsOccupied = true;
	            //m_characters[0].Move();
			}
			else
			{
				Debug.Log( "There's not enough tokens." );
			}
		}
		else
		{
			Debug.Log( "Destination Invalid!" );
		}
	}

	public bool CheckActionCost( int p_actionCost )
	{

		Debug.Log( "Action invalid!" );
		return false;
	}

	public void Attack()
	{

	}
}
