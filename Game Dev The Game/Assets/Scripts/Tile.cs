﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tile : MonoBehaviour 
{
    [SerializeField]
	private Vector2 m_gridPosition  = Vector2.zero;
    private int m_movementCost      = 1;
	private bool m_isImpassable     = false;
	private bool m_isOccupied 		= false;
	private List<Tile> m_neighbors  = new List<Tile>();

	public Vector2 GridPosition
	{
        get{ return m_gridPosition; }
        set{ m_gridPosition = value; }
	}

	public int MovementCost
	{
		get{ return m_movementCost; }
	}

	public bool IsImpassable
	{
		get{ return m_isImpassable; } 
	}

    public bool IsOccupied
	{
		get{ return m_isOccupied; }
		set{ m_isOccupied = value; }
	}

    public List<Tile> Neighbors
    {
        get{ return m_neighbors; }
        set{ m_neighbors = value; }
    }

	// Use this for initialization
	void Start () 
	{
		GenerateNeighbors();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void GenerateNeighbors()
	{
		m_neighbors = new List<Tile>();

		//Up
		if( m_gridPosition.y > 0 )
		{
			Vector2 n = new Vector2( m_gridPosition.x, m_gridPosition.y - 1 );
			m_neighbors.Add( GameManager.Instance.Map[(int)Mathf.Round(n.x)][(int)Mathf.Round(n.y)] );
		}

		//Down
		if( m_gridPosition.y < GameManager.Instance.RowNumbers - 1 )
		{
			Vector2 n = new Vector2( m_gridPosition.x, m_gridPosition.y + 1 );
			m_neighbors.Add( GameManager.Instance.Map[(int)Mathf.Round(n.x)][(int)Mathf.Round(n.y)] );
        }

		//Left
		if( m_gridPosition.x > 0 )
		{
			Vector2 n = new Vector2( m_gridPosition.x - 1, m_gridPosition.y );
			m_neighbors.Add( GameManager.Instance.Map[(int)Mathf.Round(n.x)][(int)Mathf.Round(n.y)] );
        }

		//Right
		if( m_gridPosition.x < GameManager.Instance.ColumNumbers - 1 )
		{
			Vector2 n = new Vector2( m_gridPosition.x + 1, m_gridPosition.y );
			m_neighbors.Add( GameManager.Instance.Map[(int)Mathf.Round(n.x)][(int)Mathf.Round(n.y)] );
        }
    }
    
    void OnMouseEnter()
	{
		//this.renderer.material.color = Color.green;
	}

	void OnMouseExit()
	{
		//this.renderer.material.color = Color.white;
	}

	void OnMouseDown()
	{
		if( GameManager.Instance.SelectedCharacter.IsMoving )
		{
			GameManager.Instance.MoveCharacter( this );
		}
		else
		{
			m_isImpassable = !m_isImpassable;

			if( m_isImpassable )
			{
				this.renderer.material.color = Color.magenta;
			}
			else
			{
				this.renderer.material.color = Color.white;
			}
		}
	}
}
